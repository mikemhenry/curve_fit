import numpy as np
import matplotlib.pyplot as plt

in_plane = np.genfromtxt("inplane_HW6.xls")
plt.xlabel("Distance (nm)")
plt.ylabel("Gray Value")
plt.plot(in_plane[:,0], in_plane[:,1])
plt.show()
