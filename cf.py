import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import scipy as sy
import pylab as plb
import numpy as np
import sys

def load_data(fname):

    data = np.loadtxt(fname, skiprows=1, delimiter=",")
    x_r = data[:, 0]
    y_r = data[:, 1]

    return x_r, y_r

def load_data_2(fname):
    data = np.loadtxt(fname, skiprows=1, delimiter=",")
    x_r = data[:, 2]
    y_r = data[:, 3]

    return x_r, y_r

def func(x, B, B_s, I_p, whm, x0):

    """Lorentzian functio with baselinen

    B, baseline
    B_s, baseline slope
    I_p, intensity of the peak
    whm, half width at half maximum
    x0, peak position (2theta)
    x, position (2theta)
    """

    return B + B_s * x + I_p * (whm**2/(whm**2+(x-x0)**2))


def func_no_slope(x, I_p, whm, x0):

    """Lorentzian function

    I_p, intensity of the peak
    whm, half width at half maximum
    x0, peak position (2theta)
    x, position (2theta)
    """

    return I_p * (whm**2/(whm**2+(x-x0)**2))

def data_slice(delta, peak_loc,  xarray, yarray):

    slice_range = np.where((xarray > (peak_loc - delta)) & ( xarray < (peak_loc + delta)))[0]
    slice_min = slice_range[0]
    slice_max = slice_range[-1]

    x_slice = xarray[slice_min:slice_max+1]
    y_slice = yarray[slice_min:slice_max+1]

    return x_slice, y_slice



def get_d_space(twotheta, lam):
    theta = twotheta/2
    return round(lam/(2*np.sin(np.deg2rad(theta))),4)

def get_p_size(lam, whm, twotheta):

    #print("lam", lam)

    
    theta = twotheta/2
    #print("theta", theta)
    theta_rad = np.deg2rad(theta)
    #print("theta rad", theta_rad)

    #print("B", whm)
    fwhm = 2 * whm
    fwhm_rad = np.deg2rad(fwhm)
    #print("fwhm rad", fwhm_rad)

    return (0.9 * lam)/(fwhm_rad*np.cos(theta_rad))/10


def a_hex_lattice(d_110):
    return 2*d_110

def c_hex_lattice(d_102, d_110):
    return np.sqrt(4/(1/d_102**2-1/(3*d_110**2)))

def ang_freq_pho(delta_omega):
    c = 29979245800 #cm/s
    return 2*np.pi*c*delta_omega


def k(lam):
    return 2*np.pi/lam

if __name__ == "__main__":
    fname = sys.argv[1]
    x_r, y_r = load_data(fname)

    # Guess array
    peak = sys.argv[2]
    p0 = sy.array([0, 0, 1000, 1, float(peak)])
    #p0 = sy.array([0, 0, 3000, 1, 47])
    delta = 5
    peak_loc = p0[-1]
    x, y = data_slice(delta, peak_loc, x_r, y_r)
    popt, pcov = curve_fit(func, x, y, p0)

    y_fit = func(x, *popt)
    print("B" ,popt[0])
    print("Bs", popt[1])
    print("Ip", popt[2])
    print("whm", popt[3])
    print("x0", popt[4])


    ss_res = np.dot((y - y_fit), (y - y_fit))
    ymean = np.mean(y)
    ss_tot = np.dot((y-ymean),(y-ymean))

    #print(

    #print("Correlation :",  1-ss_res/ss_tot)

    residuals = y - y_fit
    #print("RMSE",(sy.sum(residuals**2)/(residuals.size-2))**0.5)
    print("d space", round(get_d_space(popt[4], 1.5406),4))
    #print("p size (a): ", round(get_p_size(1.5405, abs(popt[3]), popt[4]),4))
    #print("a lattice:", round(a_lattice(get_d_space(popt[4], 1.5406)),4))
    #print("b lattice")



    plt.plot(x_r, y_r, markerfacecolor='none', marker='o', color='r', linewidth=0.0,  fillstyle="none", markeredgewidth = 0.5, markersize = 5)
    plt.plot(x, y_fit, color='black', linewidth=1)

    plt.show()
